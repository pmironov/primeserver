﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net.Sockets;
using System.Net;
using System.Threading;
using System.Text.RegularExpressions;
using System.IO;
using PrimeServer;

namespace HTTPServer
{
    class Program
    {
        // сервер взят с https://habrahabr.ru/post/120157/
        static void Main(string[] args)
        {
            // Определим нужное максимальное количество потоков
            // Пусть будет по 4 на каждый процессор
            int MaxThreadsCount = Environment.ProcessorCount * 4;
            // Установим максимальное количество рабочих потоков
            ThreadPool.SetMaxThreads(MaxThreadsCount, MaxThreadsCount);
            // Установим минимальное количество рабочих потоков
            ThreadPool.SetMinThreads(2, 2);
            int port = 80;
            if (args.Length == 0) // запуск из дебага
            {
                new Server(8080);
            }
            else if (Int32.TryParse(args[0], out port))
            {
                // Создадим новый сервер на указанном порту
                new Server(port);
            }
            else
            {
                Console.Write("Incorrect port number");
                Console.ReadKey();
            }
        }
    }
}
