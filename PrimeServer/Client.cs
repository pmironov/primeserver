﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;

namespace PrimeServer
{
    // Класс-обработчик клиента
    class Client
    {
        // Отправка страницы с ошибкой
        private void SendError(TcpClient Client, int Code)
        {
            // Получаем строку вида "200 OK"
            // HttpStatusCode хранит в себе все статус-коды HTTP/1.1
            string CodeStr = Code.ToString() + " " + ((HttpStatusCode)Code).ToString();
            // Код простой HTML-странички
            string Html = "<html><body><h1>" + CodeStr + "</h1></body></html>";
            // Необходимые заголовки: ответ сервера, тип и длина содержимого. После двух пустых строк - само содержимое
            string Str = "HTTP/1.1 " + CodeStr + "\nContent-type: text/html\nContent-Length:" + Html.Length.ToString() + "\n\n" + Html;
            // Приведем строку к виду массива байт
            byte[] Buffer = Encoding.ASCII.GetBytes(Str);
            // Отправим его клиенту
            Client.GetStream().Write(Buffer, 0, Buffer.Length);
            // Закроем соединение
            Client.Close();
        }

        // Конструктор класса. Ему нужно передавать принятого клиента от TcpListener
        public Client(TcpClient Client)
        {
            // Объявим строку, в которой будет хранится запрос клиента
            string Request = "";
            // Буфер для хранения принятых от клиента данных
            byte[] Buffer = new byte[1024];
            // Переменная для хранения количества байт, принятых от клиента
            int Count;
            // Читаем из потока клиента до тех пор, пока от него поступают данные
            while ((Count = Client.GetStream().Read(Buffer, 0, Buffer.Length)) > 0)
            {
                // Преобразуем эти данные в строку и добавим ее к переменной Request
                Request += Encoding.ASCII.GetString(Buffer, 0, Count);
                // Запрос должен обрываться последовательностью \r\n\r\n
                // Либо обрываем прием данных сами, если длина строки Request превышает 4 килобайта
                // Нам не нужно получать данные из POST-запроса (и т. п.), а обычный запрос
                // по идее не должен быть больше 4 килобайт
                if (Request.IndexOf("\r\n\r\n") >= 0 || Request.Length > 4096)
                {
                    break;
                }
            }

            // Задаём начальные значения
            string inputString = string.Empty;
            long input = 0;
            string output = string.Empty;
            // Парсим строку запроса с использованием регулярных выражений
            Match ReqMatch = Regex.Match(Request, @"^\w+\s+([^\s\?]+)[^\s]*\s+HTTP/.*|");

            // Если запрос не удался
            if (ReqMatch == Match.Empty)
            {
                // Передаем клиенту ошибку 400 - неверный запрос
                SendError(Client, 400);
                return;
            }

            // Получаем строку запроса
            string RequestUri = ReqMatch.Groups[0].Value;
            NameValueCollection requestParams = HttpUtility.ParseQueryString(RequestUri);
            // Получаем значение number из GET запроса
            var numberValuePosition = RequestUri.IndexOf("number");
            if (numberValuePosition < 0)
            {
                // Обращение по root или с неправильными параметрами - вернём значение по умолчанию - максимальное
                output = "Incorrect request, calculating for max number: " + Calculation(500000000000).ToString();
            }
            else
            {                
                inputString = requestParams[0].Split()[0];
                if (!Int64.TryParse(inputString, out input) || input < 1 || input > 500000000000)
                {
                    output = "Incorrect data, please enter number from 1 to 500000000000";
                }
                else
                {
                    output = Calculation(input).ToString();
                }
            }

            try
            {
                // Посылаем заголовки
                string Headers = "HTTP/1.1 200 OK\nContent-Type: text/plain\nContent-Length: " + output.Length + "\n\n";
                byte[] HeadersBuffer = Encoding.ASCII.GetBytes(Headers);
                Client.GetStream().Write(HeadersBuffer, 0, HeadersBuffer.Length);
                // Посылаем ответ
                byte[] ResponseBuffer = Encoding.ASCII.GetBytes(output);
                Client.GetStream().Write(ResponseBuffer, 0, ResponseBuffer.Length);
                Client.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        private long Calculation(long number)
        {
            // проверяем каждое следующее число 
            while (true)
            {
                number++;
                // если число простое - возвращаем
                if (IsPrime(number))
                    return number;
            }
        }

        private bool IsPrime(long n)
        {
            // 2 - простое число, сразу ответ
            if (n == 2)
                return true;
            // 1 и все чётные числа не являются простыми
            if (n == 1 || n % 2 == 0)
                return false;
            // начинаем с 3 и проверяем все нечётные числа
            for (long i = 3; i * i < n; i += 2)
            {
                // если нашли делитель - значит число не простое
                if (n % i == 0)
                    return false;
            }
            // не нашли делитель - значит число простое
            return true;
        }
    }
}

